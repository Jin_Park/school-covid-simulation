include("auxiliary.jl")
Random.seed!(0) # for reproducibility

class1 = new_classroom()
class2 = new_classroom()

class1_gif = gif_si_plot(class1, 6, 5)
class2_gif = gif_si_plot(class2, 6, 5)
infected = map(x->x.num_infected, class1)
max_infected = max(infected...)
num_infected_bar_graph = bar(0:max_infected, [count(x->x==i, infected) for i in 0:max_infected], label=false)

two_grades = two_grades_combined()
two_grades_gif = gif_si_plot(two_grades, 15, 5)

two_grades2 = two_grades_combined()
two_grades_gif2 = gif_si_plot(two_grades2, 15, 5)

gymnasium = new_classroom(d=2)
gym_gif = gif_si_plot(gymnasium, 11, 9)

gymnasium2 = new_classroom(d=2)
gym_gif2 = gif_si_plot(gymnasium2, 11, 9)

one_grade_mean = plot_simulation_mean(6, 5, 100000, NoRecoveryInfection(0.01))
two_grades_mean = plot_simulation_mean(15, 5, 100000, NoRecoveryInfection(0.01), two_grades=true)
