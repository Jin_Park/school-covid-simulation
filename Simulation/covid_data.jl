function plot_covid()
    url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv";
    download(url, "covid_data.csv");
    csv_data = CSV.File("covid_data.csv");
    data = DataFrame(csv_data)
    countries = data[:, "Country/Region"]
    korea = findfirst(x->x=="Korea, South", countries)
    # plot(Vector(data[korea, 5:end]), label="Daily Cases", xlabel="Days past 1/22/20")
    korea_covid = Vector(data[korea, 5:end])
    korea_covid_daily = zeros(length(korea_covid))
    for i in 2:length(korea_covid)
        korea_covid_daily[i] = korea_covid[i] - korea_covid[i - 1]
    end

    covid_daily_plot = bar(korea_covid_daily, label=false, title="Days past 1/22/20")
    hline!(covid_daily_plot, [korea_covid_daily[end]], linestyle=:dash, label=false)
    return (plot=covid_daily_plot, daily_data = korea_covid_daily)
end
